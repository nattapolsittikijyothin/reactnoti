import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { askForPermissioToReceiveNotifications } from './push-notification';
class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      token: null
    }
  }
  askForPermissioToReceiveNotifications = () => {
    const deferred = askForPermissioToReceiveNotifications();
    deferred.then(token =>{
      
      this.setState({
        token: token
      });
    })
    
  };

  render() {
    const { token } = this.state;

    console.log("render ",token);
    return (
      <div className="App">
        <p>  {token}</p>
        <header className="App-header">
          <button onClick={this.askForPermissioToReceiveNotifications.bind(this)}>Clickme</button>
          <textarea rows="4" cols="50">
            {token}
          </textarea>
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  }
}

export default App;
