import firebase from 'firebase';

export const initializeFirebase = () => {
  firebase.initializeApp({
    apiKey: "AIzaSyCzNs6NykIa8tRoiHYGhkEePOdcXo25fhE",
    authDomain: "quickpm-noti.firebaseapp.com",
    databaseURL: "https://quickpm-noti.firebaseio.com",
    projectId: "quickpm-noti",
    storageBucket: "quickpm-noti.appspot.com",
    messagingSenderId: "571360232134"
  }); 

  navigator.serviceWorker.register('firebase-messaging-sw.js')
    .then((registration) => { 
    console.log('tryna register service:', registration);
      firebase.messaging().useServiceWorker(registration);
    });
}

export const askForPermissioToReceiveNotifications = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
    const token = await messaging.getToken();
    console.log('token do usuário:', token);
    
    return token;
  } catch (error) {
    console.error("Error naja : ",error);
  }
}